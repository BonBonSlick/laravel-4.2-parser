<hr>
@if(isset($comments) && count($comments))
	<h4>Комментрарии:</h4>
	<div class="comments-cont">
		@foreach($comments as $comment)
			@include('content.front.comment')
		@endforeach
	</div>
	{{ $comments->links() }}
@else
	<div class="comments-cont">
		<div class="none-comments">
			Комментарии отсутствуют
		</div>
	</div>
	<hr>
@endif

{{ Form::open(array('role' => 'form', 'url' => 'http://serv2.timworld.info/comments/store', 'onsubmit' => 'return checkform();')) }}
<div class="comment-form">
	<div class="comment-user-form">
		<div class='form-group'>
		    {{ Form::label('email', 'Email*', array('class' => 'pull-left')) }}
		    <div>
		    	{{ Form::text('email', null) }}
		    </div>
		</div>
	</div> 
	<div class='form-group'>
		{{ Form::label('content', 'Оставить комментарий (до 1000 символов)') }}
	    {{ Form::textarea('content', null, array('class' => 'comment-textarea')) }}
	    {{ Form::hidden('itemId',  $item->id) }}
	    {{ Form::hidden('table',  $item->table) }}
	</div>
	<div class='form-group'>
		<div class="g-recaptcha" data-sitekey="6LemkyETAAAAAAdbRjColMY_S8zKfAc-80kU_O3J"></div>
	</div>
	<div class='form-group'>
	    {{ Form::button('Отправить', array('class' => 'comment-submit button', 'type' => 'submit')) }}
	</div>
</div>
{{ Form::close() }}