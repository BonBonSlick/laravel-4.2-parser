@extends('containers.frontend')
@section('title')Календарь событий @stop
@section('styles')
    <link rel='stylesheet' href='/assets/packs/fullcalendar/fullcalendar.min.css'>
    <style type="text/css">.fc-event-container{ cursor: pointer; } </style>
@stop
@section('scripts')
    <script src='/assets/packs/fullcalendar/lib/moment.min.js'></script>
    <script src='/assets/packs/fullcalendar/fullcalendar.js'></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#calendar').fullCalendar({
                selectable: false,
                editable: false,
                eventLimit: true,
                events: [
                   @if(count($events))
                        @foreach($events as $key => $event)
                            {
                                id: '{{ $event->id }}',
                                title: '{{ str_limit($event->subject, 5) }}',
                                start: '{{ date('Y-m-d H:i:s',strtotime($event->date)) }}',
                                url: '/calendar/{{ $event->id }}'
                            }{{ isset($events[$key+1])?',':'' }}
                        @endforeach
                    @endif
                ]
            })
        });
    </script>
@stop
@section('main')
	<div class="row">
		<div class="col-md-8">
            <div class="heading">Календарь событий</div>
			<div id='calendar'></div>
		</div>
		<div class="col-md-4">
			@include('content.front.rightblock')
		</div>
	</div>
@stop