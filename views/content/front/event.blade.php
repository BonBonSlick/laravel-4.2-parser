@extends('containers.frontend')
@section('title'){{$event->subject}} @stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="/assets/packs/flipclock/flipclock.css?v={{time()}}"/ >
@stop
@section('scripts')
    <script src="/assets/packs/flipclock/flipclock.min.js?v={{time()}}"></script>
@stop
@section('main')
	<div class="row">
		<div class="col-md-8">
			<h1 class="text-center">{{$event->subject}}</h1>
            <br>
            <div class="flipclock" style="left: 40px;"></div>
            <script type="text/javascript">
                var clock = jQuery('.flipclock').FlipClock('@if($interval>0){{$interval}} @else 0 @endif',{
                    clockFace: 'DailyCounter',
                    countdown: true
                });
            </script> 
            <div class="text-justify event-message">
                @if(!empty($event->image))
                    <img src="/{{ $event->image }}" style="max-width:300px; margin: 0 10px 0 0;" class="pull-left article_main_image" property="image">
                @endif

               {{$event->message}}
            </div>
		</div>
		<div class="col-md-4">
			@include('content.front.rightblock')
		</div>
	</div>
@stop