@if($vote)
<div class="votes-cont">
	<div class="heading votes-heading" style="cursor:pointer"><i class="fa fa-plus-circle"></i> Голосования</div>
	<div class="votes" style="display:none">
	<div class="panel">
		{{$vote}}
	</div>
	<script>
		$(document).ready(function(){
			setTimeout(function(){
			  	$('.votes').slideToggle(1000);
				$('.votes-heading i').toggleClass('fa-minus-circle');
			}, 1000);
			
			$('.votes-heading').on('click',function(){
				$(this).parent().find('.votes').slideToggle();
				$(this).find('i').toggleClass('fa-minus-circle');
			});

			$('#vote_question').on('click',function(){
				if ($('input[name=answer]', '#answers').is(':checked'))
				{
		           	$.ajax({
		                type: "POST",
		                url: '/admin/votes/processvote',
		                data: {
		                	question: $(this).data('question'),
		                	answer: $('input[name=answer]:checked', '#answers').val(),
		                },
		                success: function( response ) {
		                    $('.votes .panel').html(response);
		                   	$('#vote_question').fadeOut();
		                }
		            })
	           	}
	        });
			
		});
	</script>
</div>
@endif