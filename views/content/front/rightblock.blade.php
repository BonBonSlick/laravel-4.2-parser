@if(isset($weeklyNews)&&!empty($weeklyNews))
	<div class="heading">Итоги недели</div>
	@foreach($weeklyNews as $article)
		@include('content.front.articlelistitem')
	@endforeach	
@endif

@if(isset($crimea)&&!empty($crimea))
	<div class="heading">Новости Крыма</div>
	@foreach($crimea as $article)
		@include('content.front.articlelistitem')
	@endforeach	
@endif

@if(Request::is('/'))
	@if(isset($accidentNews)&&!empty($accidentNews))
		<div class="heading">Происшествия</div>
		@foreach($accidentNews as $article)
			@include('content.front.articlelistitem')
		@endforeach
	@endif
@endif
<div class="text-center" style="margin: 20px 0">

</div>

<div class="heading">Наши партнёры</div>
<!-- MFatlink -->
<?php
	if(isset($_GET['asd'])){
		global $FatlinkClient;                                                 
		include_once($_SERVER['DOCUMENT_ROOT'] . '/ftlnk/fatlink.php');	 
		$FatlinkClient->view();
	}
  ?>
<!-- MarketGidNews Start -->
<div id="MarketGidScriptRootN1801" class="news-block-magick">
    <div id="MarketGidPreloadN1801">
        <a id="mg_add1801" href="http://usr.marketgid.com/demo/celevie-posetiteli/" target="_blank"><img src="//cdn.marketgid.com/images/marketgid_add_link.png" style="border:0px"></a><br>        <a href="http://marketgid.com/" target="_blank">Загрузка...</a>    
    </div>
    <script>
                        (function(){
            var D=new Date(),d=document,b='body',ce='createElement',ac='appendChild',st='style',ds='display',n='none',gi='getElementById';
            var i=d[ce]('iframe');i[st][ds]=n;d[gi]("MarketGidScriptRootN1801")[ac](i);try{var iw=i.contentWindow.document;iw.open();iw.writeln("<ht"+"ml><bo"+"dy></bo"+"dy></ht"+"ml>");iw.close();var c=iw[b];}
            catch(e){var iw=d;var c=d[gi]("MarketGidScriptRootN1801");}var dv=iw[ce]('div');dv.id="MG_ID";dv[st][ds]=n;dv.innerHTML=1801;c[ac](dv);
            var s=iw[ce]('script');s.async='async';s.defer='defer';s.charset='utf-8';s.src="//jsn.marketgid.com/u/i/uinp.info.1801.js?t="+D.getYear()+D.getMonth()+D.getDate()+D.getHours();c[ac](s);})();
    </script>
</div>
<!-- MarketGidNews End -->

<!--  News Join -->

<div class="join_informer_3584" id="join_informer_3584"></div>

<!-- Асинхронный код, 21.010.2015 -->
<script type="text/javascript">  (function() {
var i = document.createElement("script"); i.type = "text/javascript";
i.async = true; i.charset = "UTF-8";
i.src = "http://partner.join.com.ua/async/3584/";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(i, s);
})();</script>
<!--  News Join -->

<?php 
include_once($_SERVER['DOCUMENT_ROOT'].'/webgarden/webgarden_code.php');  
$o['USERNAME'] = 'B089BEB373FF33D7BF49FB003B116A52'; 
$client_lnk = new WebGardenInit($o); 
// Вивід вічних посилань  
echo $client_lnk->ShowFLinks();  
// Відступ від блоку  
echo "<p></p>";  
// Вивід посилань  
echo $client_lnk->ShowLinks();  
?>

@include('content.front.socwidgets')
