<center><h4><b>{{ $question->question }}  ?</b></h4></center>
<form id="answers">
	@foreach ($answers_id as $answer)
	 	<div class="radio">
		  	<label>
		    	<input type="radio" name="answer" value="{{$answer->id}}">
		    	{{$answer->answer}}
		  	</label>
		</div>
	@endforeach
</form>
<center><button id="vote_question" data-question="{{$question->id}}" class="btn btn-success btn-xs">Проголосовать</button></center><br>