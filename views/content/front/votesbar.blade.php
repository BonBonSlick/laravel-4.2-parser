<label>{{$answer_name}}</label>
<div class="progress" style="margin-bottom: 5px;">
    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$percent_bar}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percent_bar}}%">{{$percent_bar}}%</div>
</div>