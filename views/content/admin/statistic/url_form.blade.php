@extends('containers.admin')
 
@section('title') Edit muble @stop
 
@section('main')

    @if (Request::segment(3)=='createurl')
        {{ Form::open(array('role' => 'form', 'url' => 'admin/statistic/storeurl')) }}
    @else
        {{ Form::model($url, array('role' => 'form', 'url' => 'admin/statistic/updateurl/' . $url->id, 'method' => 'PUT')) }}
    @endif
 
    <div style="overflow: hidden;">
        <h1 class="fa fa-bar-chart"> {{Request::segment(3)=='createurl'?'Add':'Edit'}} URL</h1>

        <div class='form-group pull-right top20'>
            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class='form-group '>
                {{ Form::label('url', 'URL') }}
                {{ Form::text('url', null, array('placeholder' => 'url', 'class' => 'form-control')) }}
            </div>
        </div>
    </div>
  
    {{ Form::close() }}

@stop
