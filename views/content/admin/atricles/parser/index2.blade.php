@extends('containers.admin')

@section('styles')
    <style>
        .table th{
            min-width:0px;
        }
    </style>
@stop

@section('main')

    <div style="overflow:hidden">
    <h1 class="fa fa-gear pull-left"> parser2 Data</h1>
    {{ link_to('admin/parser2/create/', 'Add new url', array('class'=>'pull-right btn btn-primary top20 left10')) }}

    <?php 
        $search_fields = array_flip($table_fields);
        $search_fields = array_combine($search_fields, $search_fields);
    ?>
    {{ Form::open(array('role' => 'form', 'url' => 'admin/parser2', 'method' =>'get', 'class' => 'pull-right top20 table-search-form')) }}
        {{ Form::select('field',$search_fields,'',array('class'=>'form-control pull-left')) }} 
        {{ Form::text('search','',array('class'=>'form-control pull-left')) }}
        {{ Form::submit('Search',array('class'=>'btn btn-info')) }}
    {{ Form::close() }}
    </div>  

    <div class="panel panel-info">
        <div class="panel-heading">Parser controls</div>
        <div class="panel-body">
            <form action="/admin/parser2/parse" method="POST">
                <div class="col-md-2">
                    <label>Parse Direction</label>
                    <select name="direction" class="form-control" style="width:120px">
                        <option value="desc">From last</option>
                        <option value="asc">From first</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Timeout (sek)</label>
                    <input type="text" value="6000" class="form-control" style="width:100px">
                </div>
                <div class="col-md-2">
                    <label>Parser status</label>
                    <div>{{ $workTime }}</div>
                </div>
                <div class="col-md-2">
                    <label>Parser started at</label>
                    <div>{{ $parserStarted!='0000-00-00 00:00:00'?$parserStarted:'None' }}</div>
                </div>
                <div class="col-md-2" style="padding-top:10px">
                    <a href="/admin/parser2/reset" class="pull-right btn btn-warning form-control">Reset parser</a>
                </div>
                <div class="col-md-2" style="padding-top:10px">
                    <input type="submit" value="Parse" class="pull-right btn btn-success form-control">
                </div>
            </form>
        </div>
    </div>  

    @if ($data->count())
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    {{ Common_helper::sorting_table_fields($table_fields) }}
                    <th style="width: 320px;">Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->url }}</td>
                        <td>{{ $item->publish?'<span class="fa fa-check">':'<span class="fa fa-times">' }}</td>
                        <td>{{ $item->translate?'<span class="fa fa-check">':'<span class="fa fa-times">' }}</td>
                        <td>{{ $item->disabled?'<span class="fa fa-check">':'<span class="fa fa-times">' }}</td>
                        <td>{{ $item->username }}</td>
                        <td>
                        	<form action="/admin/parser2/parse" method="POST">
                                <input type="hidden" name="preview" value="true">
                                <input type="hidden" name="parser_id" value="{{ $item->id }}">
                                <input type="submit" value="Preview" class="btn btn-success btn-xs pull-left left10">                                
                            </form> 
                            <form action="/admin/parser2/parse" method="POST">
                                <input type="hidden" name="parser_id" value="{{ $item->id }}">
                                <input type="hidden" name="test_links" value="{{ $item->id }}">
                                <input type="submit" value="Test links" class="btn btn-success btn-xs pull-left left10">
                            </form>
                            <form action="/admin/parser2/parse" method="POST">
                                <input type="hidden" name="parser_id" value="{{ $item->id }}">
                                <input type="hidden" name="test" value="1">
                                <input type="submit" value="Test" class="btn btn-success btn-xs pull-left left10">                                
                            </form>
                            {{ link_to('admin/parser2/edit/'.$item->id, 'Edit', array('class' => 'btn btn-info btn-xs pull-left left10', 'title'=>'edit')) }}
                            {{ Form::open(array('url' => 'admin/parser2/destroy/' . $item->id, 'method' => 'DELETE')) }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs left10','onclick'=>'return confirm(\'Delete item?\')?true:false;'))}}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $data->links() }}    
    @endif

@stop