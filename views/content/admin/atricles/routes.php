<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Route::controller('/auth', 'AuthController');

/*
|--------------------------------------------------------------------------
| For all
|--------------------------------------------------------------------------
*/
Route::post('comments/store','CommentController@postStore');
Route::controller('cron', 'CronController');
Route::get('/sitemap', 'FrontController@sitemap');
Route::post('admin/votes/processvote','VoteController@processvote');
/*
|--------------------------------------------------------------------------
| For registered users
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'auth'), function() {
     Route::controller('comments','CommentController');     
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'is_admin'), function() {	
     Route::get('admin', 'DashboardController@getIndex');
     Route::post('admin/dashboard/parents_links', 'MenuController@parentsLinks');
     Route::post('admin/dashboard/tree_list', 'DashboardController@treeList');

     Route::get('admin/parser2-word-blacklist', 'Parser2Controller@blacklist');
     Route::get('admin/parser2-word-blacklist/create', 'Parser2Controller@blacklistWordCreatePage');
     Route::post('admin/parser2-word-blacklist/store', 'Parser2Controller@blacklistWordCreate');
     Route::get('admin/parser2-word-blacklist/edit/{id}', 'Parser2Controller@blacklistWordEdit');
     Route::put('admin/parser2-word-blacklist/update/{id}', 'Parser2Controller@blacklistWordUpdate');
     Route::get('admin/parser2-word-blacklist/search', 'Parser2Controller@blacklistWordSearch');
     Route::delete('admin/parser2-word-blacklist/destroy/{id}', 'Parser2Controller@blacklistWordDelete');
     Route::get('admin/parser/parse/{parserId?}', 'ParserController@getParse'); 
     Route::post('admin/parser2/parse','Parser2Controller@parse');

     Route::controller('admin/seo', 'SeoController');
     Route::controller('admin/folders', 'FolderController');
     Route::controller('admin/articles', 'ArticleController');
     Route::controller('admin/comments','CommentController');
     Route::controller('admin/tags','TagController');
     Route::controller('admin/menus', 'MenuController');
     Route::controller('admin/users', 'UserController');
     Route::controller('admin/parser', 'ParserController');  
     Route::controller('admin/parser2', 'Parser2Controller');
     Route::controller('admin/mumble', 'MumbleController');
     Route::controller('admin/statistic', 'StatisticController');  

     Route::controller('admin/votes', 'VoteController');
     Route::controller('admin/events', 'EventController');  
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/
            
Route::controller('/', 'FrontController');
Route::get('/calendar/{event_id}', 'FrontController@getCalendar');
