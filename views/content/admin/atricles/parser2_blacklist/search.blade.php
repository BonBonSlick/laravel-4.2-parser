@extends('containers.admin')
@section('main')

<h1 class="fa fa-edit pull-left"> All Parser2 Blacklist Words</h1>
{{ link_to('admin/parser2-word-blacklist/create', 'Add new word', array('class'=>'pull-right btn btn-primary top20 left10')) }}

  {{ Form::open(array('role' => 'form', 'url' => 'admin/parser2-word-blacklist/search', 'method' =>'get', 'class' => 'pull-right top20 table-search-form')) }}
        {{ Form::text('search','',array('class'=>'form-control pull-left', 'placeholder="Type word"')) }}
        {{ Form::submit('Search',array('class'=>'btn btn-info')) }}
    {{ Form::close() }}

    <div class="col-md-12 text-right">
        <small>
            (Key sensetive, word "Admin" != "admin")
        </small>
    </div>

    @if ($words)
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Word</th>
                    <th style="min-width: 130px;">Actions</th>
                </tr>
            </thead>
                <tbody>
                @foreach ($words as $word)
                    <tr>
                        <td>
                            {{ $word->word }}
                        </td>
                        <td>
                            {{ link_to('admin/parser2-word-blacklist/edit/'.$word->id, 'Edit', array('class' => 'btn btn-info btn-xs pull-left left10', 'title'=>'edit')) }}
                            {{ Form::open(array('url' => 'admin/parser2-word-blacklist/destroy/' . $word->id, 'method' => 'DELETE')) }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs left10','onclick'=>'return confirm(\'Delete word?\')?true:false;')) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $words->links() }}
    @endif
@stop

