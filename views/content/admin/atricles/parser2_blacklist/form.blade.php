@extends('containers.admin')

@section('title') Create Parser2 Blacklist Word @stop

@section('main')

<div class='col-4 col-offset-4'>

    @if (Request::segment(3)=='create')
        {{ Form::open(array('role' => 'form', 'url' => 'admin/parser2-word-blacklist/store')) }}
    @else
        {{ Form::model($word, array('role' => 'form', 'url' => 'admin/parser2-word-blacklist/update/' . $word->id, 'method' => 'PUT')) }}
    @endif

    <div style="overflow: hidden;">
        <h1 class="fa fa-edit pull-left"> {{Request::segment(3)=='create'?'Add':'Edit'}} word</h1>

        <div class='form-group pull-right top20 left10'>
            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>

     <div class='form-group'>
            {{ Form::label('word', 'Blacklist Word:') }}
            {{ Form::text('word', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::close() }}

</div>

@stop
