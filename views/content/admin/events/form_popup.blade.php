<form>
	<div class="form-group">
		<label>Date</label>
		<input type="text" name="date" class="form-control" readonly value="<?php echo isset($startDate)?$startDate:'' ?>">
	</div>
	<div class="form-group">
		<label>Subject</label>
		<input type="text" id="event_subject" name="subject" class="form-control">
	</div>
	<div class="form-group">
		<label>Message</label>
		<textarea class="form-control" name="message" style="max-width:100%"></textarea>
	</div>
	<div class="form-group">
		<label>Show to customer</label>
		<select class="form-control" name="public">
			<option value="0">No</option>
			<option value="1">Yes</option>
		</select>
	</div>
	<div class="form-group text-center">
		<input type="button" value="Submit" class="btn btn-default save_event">
	</div>
</form>