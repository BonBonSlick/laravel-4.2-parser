@extends('containers.admin')
 
@section('title') {{Request::segment(3)=='create'?'Add':'Edit'}} Event @stop

@section('styles')
    <link rel="stylesheet" type="text/css" href="/assets/packs/datetimepicker/jquery.datetimepicker.css"/ >
@stop

@section('scripts')
    <script src="/assets/packs/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
    <script type="text/javascript" src="/packages/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        jQuery('.datetimepicker').datetimepicker({format:'Y-m-d H:i'});
        tinymce.init({
            selector: ".message-textarea",
            height : 300,
            relative_urls: false,
            plugins: [
                "advlist autolink lists link image responsivefilemanager charmap print preview anchor textcolor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste image_upload, pagebreak"
            ],
            pagebreak_separator: "<pagebreak>",
            //outdent indent
            toolbar: "insertfile undo redo | styleselect | fontsizeselect | fontselect | backcolor | forecolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image image_upload",
            relative_urls: false,
            forced_root_block : 'div',
            image_title: true,
            style_formats: [
                { title: 'Headers', items: [
                  { title: 'h1', block: 'h1' },
                  { title: 'h2', block: 'h2' },
                  { title: 'h3', block: 'h3' },
                  { title: 'h4', block: 'h4' },
                  { title: 'h5', block: 'h5' },
                  { title: 'h6', block: 'h6' }
                ] },

                { title: 'Blocks', items: [
                  { title: 'p', block: 'p' },
                  { title: 'div', block: 'div' },
                  { title: 'pre', block: 'pre' }
                ] },

                { title: 'Containers', items: [
                  { title: 'blockquote', block: 'blockquote', wrapper: true, styles: { 'background':'url("/assets/images/quotes.png") 7px 7px / 30px no-repeat #eaf5ec','padding':'10px 10px 10px 50px','margin':'10px 0','border-left':'5px solid #7ABD77','overflow':'hidden' } },
                  { title: 'conclusion', block: 'blockquote', wrapper: true, styles: { 'background':'url("/assets/images/conclusion.png") 7px 7px / 30px no-repeat #d9edf7','padding':'10px 10px 10px 50px','margin':'10px 0','border-left':'5px solid #67D5E6','overflow':'hidden' } },
                  { title: 'border div green', block: 'div', wrapper: true, styles: { 'border':'solid 4px #eaf5ec','padding':'10px','margin':'10px 0','overflow':'hidden' } },
                  { title: 'border div blue', block: 'div', wrapper: true, styles: { 'border':'solid 4px #d9edf7','padding':'10px','margin':'10px 0','overflow':'hidden' } },
                ] }
            ],
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function(){
            $('body').on('change','#imgInp',function(){
                readURL(this); 
            });

            $('.delete_image').on('click',function(){
                $('#img_preview').attr('src','/assets/images/no-image.jpg');
                $('#imgInp').val('');
                $('#image_path').val('');
                $('#image_thumb').val('');
            });
        });
    </script>
@stop
 
@section('main')
    @if (Request::segment(3)=='create')
        {{ Form::open(array('role' => 'form', 'url' => 'admin/events/store', 'files' => true)) }}
    @else
        {{ Form::model($event, array('role' => 'form', 'url' => 'admin/events/update/' . $event->id, 'method' => 'PUT', 'files' => true)) }}
    @endif

    <h1 class='fa fa-calendar'> {{Request::segment(3)=='create'?'Add':'Edit'}} Event</h1>

    <div class="pull-right" style="margin-top:20px">
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
 
    <div class="form-group">
        {{ Form::label('Date', 'Date') }}
        {{ Form::text('date', null, array('class' => 'form-control datetimepicker')) }}
    </div>
    <div class="form-group">
        {{ Form::label('subject', 'Subject') }}
        {{ Form::text('subject', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('message', 'Message') }}
        {{ Form::textarea('message', null, array('class' => 'form-control message-textarea','style' => 'max-width:100%')) }}
    </div>

    <div class="row">
        <div class='form-group col-md-8'>
            <label>Show to customer</label>
            <select class="form-control" name="public">
                <option value="0">No</option>
                <option value="1" <?php echo !empty($event->public)?'selected':'' ?>>Yes</option>
            </select>             
        </div>
        <div class='form-group col-md-4'>        
            <div class="panel panel-default">
                <div class="panel-heading">Media <i class="fa fa-times delete_image pull-right" title="Delete"></i></div>
                <div class="panel-body">
                    <div class="upload_img_cont form-group">                                             
                        <img id="img_preview" src="{{ !empty($event->image)?'/'.$event->image:'/assets/images/no-image.jpg' }}" alt="your image" /><br><br>
                        {{ Form::file('image', array('id' => 'imgInp')) }}
                        <input type="hidden" name="image_status" id="image_path" value="{{ !empty($event->image)?$event->image:'' }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
 
@stop
