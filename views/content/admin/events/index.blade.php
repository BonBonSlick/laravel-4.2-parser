@extends('containers.admin')
@section('title') Events @stop
@section('styles')
    <link rel='stylesheet' href='/assets/packs/fancyBox/jquery.fancybox.css' />
    <link rel='stylesheet' href='/assets/packs/fullcalendar/fullcalendar.min.css' />
@stop
@section('scripts')
    <script src='/assets/packs/fancyBox/jquery.fancybox.js'></script>
    <script src='/assets/packs/fullcalendar/lib/moment.min.js'></script>
    <script src='/assets/packs/fullcalendar/fullcalendar.js'></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('body').on('click','.save_event',function(){
                var form = $( this ).parent().parent();
                var title = form.find('#event_subject');
                $.ajax({
                    url: '/admin/events/save',
                    type: 'post',
                    data: {
                        data: form.serialize()
                    },
                    success:function(res){
                        addEvent(title.val(),res);
                        $.fancybox.close();
                    }
                })
            })

            function removeEvent(calEvent){
                $.ajax({
                    url: '/admin/events/remove',
                    type: 'post',
                    data: {
                        id: calEvent.id
                    },
                    success:function(res){
                        $('#calendar').fullCalendar('removeEvents',calEvent._id);
                    }
                })
            }

            function addEvent(title,id){
                var eventData;
                if (title) {
                    eventData = {
                        id: id,
                        title: title,
                        start: startDate,
                        end: endDate
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                }
                $('#calendar').fullCalendar('unselect');
            }
            
            $('#calendar').fullCalendar({
                selectable: true,
                select: function(start, end) {                    
                    startDate = start;
                    endDate = end;
                    $.fancybox({
                        'type': 'ajax',
                        'href': '/admin/events/form/'+encodeURIComponent(start.format('YYYY-MM-DD HH:mm:ss'))
                    });
                },
                eventRender: function(event, element) {
                    element.find('.fc-content').append("<i style='float:right;padding-top:2px;color:black;cursor: pointer;' class='fa fa-trash'></i><i onclick='window.open(\"/admin/events/edit/"+event.id+"\")' style='float:right;padding-top:2px;padding-right:5px;color:black;cursor: pointer;' class='fa fa-pencil'></i>");
                    element.find('.fc-content').find(".fa-trash").click(function() {
                        removeEvent(event);
                    });
                },
                editable: false,
                eventLimit: true,
                events: [
                   @if(count($events))
                        @foreach($events as $key => $event)
                            {
                                id: '{{ $event->id }}',
                                title: '{{ str_limit($event->subject, 10) }}',
                                start: '{{ date('Y-m-d H:i:s',strtotime($event->date)) }}',//'2015-02-16T16:00:00'
                            }{{ isset($events[$key+1])?',':'' }}
                        @endforeach
                    @endif
                ]
            })
        });
    </script>
@stop

@section('main')
    <h1 class="fa fa-calendar"> Events</h1>
    {{ link_to('admin/events/create/', 'Add new event', array('class'=>'pull-right btn btn-primary top20 left10')) }}
    <br>
    <div id='calendar'></div>
    <br>
@stop