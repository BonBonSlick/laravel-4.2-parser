@extends('containers.admin')

@section('main')

    <h1 class="fa fa-question-circle"> All Votes</h1>
    {{ link_to('admin/votes/create/', 'Add new vote', array('class'=>'pull-right btn btn-primary top20 left10')) }}

    <?php 
        $search_fields = array_flip($table_fields);
        $search_fields = array_combine($search_fields, $search_fields);
    ?>
    {{ Form::open(array('role' => 'form', 'url' => 'admin/votes', 'method' =>'get', 'class' => 'pull-right top20 table-search-form')) }}
        {{ Form::select('field',$search_fields,Input::get('field'),array('class'=>'form-control pull-left')) }} 
        {{ Form::text('search',Input::get('search'),array('class'=>'form-control pull-left')) }}
        {{ Form::submit('Search',array('class'=>'btn btn-info')) }}
    {{ Form::close() }}  

    @if ($votes->count())
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    {{ Common_helper::sorting_table_fields($table_fields) }}
                    <th style="min-width: 50px;"><center>Actions</center></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($votes as $vote)
                    <tr @if($vote->active == 1) class="success" @endif>
                        <td>{{ $vote->id }}</td>
                        <td>{{ $vote->question }}</td>
                        <td>{{ $vote->created_at }}</td>
                        <td>{{ $vote->updated_at }}</td>
                        <td>
                            {{ link_to('admin/votes/edit/'.$vote->id, 'Edit', array('class' => 'btn btn-info btn-xs pull-left left10', 'title'=>'edit')) }}
                            {{ Form::open(array('url' => 'admin/votes/destroy/' . $vote->id, 'method' => 'DELETE')) }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-xs left10','onclick'=>'return confirm(\'Delete vote?\')?true:false;'))}}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $votes->links() }}
    @else
        <div class="alert alert-info">Votes not found!</div>
    @endif

@stop