@extends('containers.admin')
 
@section('title') Questions @stop

@section('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.add_row_btn').on('click',function(){
                $('.rows_block').append('@include('content.admin.votes.row')');
            })

            $('body').on('click','.remove_row',function(){
                $(this).parent().parent().remove()
            })
        })
    </script>
@stop

@section('main')
     <style type="text/css">.answer_row div {margin-bottom: 10px;}</style>

    @if (Request::segment(3)=='create')
        {{ Form::open(array('role' => 'form', 'url' => 'admin/votes/store')) }}
    @else
        {{ Form::model($question, array('role' => 'form', 'url' => 'admin/votes/update/' . $question->id, 'method' => 'PUT')) }}
    @endif

    <h1 class='fa fa-question-circle'> {{Request::segment(3)=='create'?'Add':'Edit'}} vote</h1>

    <div class="pull-right" style="margin-top:20px">
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>

    <div class="row" style="padding-top:20px">
        <div class="col-md-6">
            <div class="form-group">
                {{ Form::label('question', 'Question') }}
                {{ Form::text('question', null, array('placeholder' => 'Question', 'class' => 'form-control')) }}
                <br>
                {{ Form::label('active', 'Active') }}
                Yes {{ Form::radio('active', 1) }}
                No  {{ Form::radio('active', 0) }}
            </div>
        </div>
        <div class="col-md-12 rows_block" style="padding-top:10px;">
            {{ Form::label('answers', 'Answers') }}
            @if(isset($rows)&&!empty($rows))
                @foreach($rows as $row)
                    @include('content.admin.votes.row')
                @endforeach
            @else 
               @include('content.admin.votes.row') 
            @endif
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <span class="btn btn-info add_row_btn">Add Row</span>
            </div>
        </div>
    </div>
 
    {{ Form::close() }}
 
@stop
